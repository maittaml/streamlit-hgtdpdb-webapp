### standard
import streamlit as st
from core.Page import Page
### custom
import pandas as pd
import ast
import csv
import json
import plotly.graph_objects as go
from datetime import datetime
### PDB stuff
import itkdb
import core.DBaccess as DBaccess
import core.stInfrastructure as infra
import itkdb
import itkdb.exceptions as itkX

#####################
### useful functions
#####################
def SelectCheck(k,v):
    val=None
    if "img" in k.lower(): # for images (identified by name for the moment)
        val=st.file_uploader(k+" (image):", type=["png", "jpg", "jpeg"]) #bytesio for binary uploads
    try: # for bools formatted as strings by schema
        if "true" in v.lower() or "false" in v.lower():
            val=st.radio(k+" :", [True, False], index=["True", "False"].index(v))
        else:
            val=st.text_input(k+" :", value=str(v), max_chars=None, key=None, type='default')
            if "[" and "]" in val or "{" and "}" in val:
                val=ast.literal_eval(val)
    except AttributeError:
        if type(v)==type(True):
            val=st.radio(k+" :", [True, False], index=[True, False].index(v))
        else:
            val=st.text_input(k+" :", value=str(v), max_chars=None, key=None, type='default')
            if "[" and "]" in val or "{" and "}" in val:
                val=ast.literal_eval(val)
    try: # avoid bool trouble
        if val.isnumeric(): # for integers formatted as strings by schema
            val=int(val)
    except AttributeError:
        pass
    return val

def ColorCells(s, df, colName, flip=False):
    thisRow = pd.Series(data=False, index=s.index)
    colours=['red','blue','green','orange','purple''yellow','pink','lightblue','lightgreen']*3
    names=list(df[colName].unique())
    if flip:
        return ['background-color: %s ; color: %s'% ('white',colours[names.index(s[colName])])]*len(df.columns)
    else:
        return ['background-color: %s ; color: %s'% (colours[names.index(s[colName])],'black')]*len(df.columns)

def EditJson(inJson):
    for k,v in inJson.items():
        if type(v)==type({}):
            st.write("**"+str(k)+"**")
            for l,w in v.items():
                inJson[k][l]=SelectCheck(l,w)
        else:
            inJson[k]=SelectCheck(k,v)
    return inJson


infoList=["  * check settings for testType",
        "  * get test schema",
        "  * review test schema",
        "   * reset if required",
        "   * edit if required",
        "  * upload test schema",
        "   * delete test upload if required"]
#####################
### main part
#####################

class Page2(Page):
    def __init__(self):
        super().__init__("Test Data Upload", ":microscope: Upload Test Information", infoList)

    def main(self):
        super().main()

        ### getting attribute
        pageDict=st.session_state[self.name]

        ### check requirements to do stuff
        doWork=False
        try:
            if st.session_state.myClient:
                doWork=True
            if st.session_state.debug:
                st.write(":white_check_mark: Got Token")
        except AttributeError:
            st.write("No token")

        ### gatekeeping
        if not doWork:
            st.stop()

        # set up test info.
        if "componentType" not in pageDict.keys():
            pageDict['componentType']="MODULE"
        if "code" not in pageDict.keys():
            pageDict['code']="WIREBOND"
        if "stage" not in pageDict.keys():
            pageDict['stage']="MODULEWIREBONDING"
        if "project" not in pageDict.keys():
            pageDict['project']="P"

        ### test parameters
        st.write("### TestType info.")
        st.write("_Current_ settings")
        st.write("componentType (code):",pageDict['componentType'])
        st.write("testType (code):",pageDict['code'])
        st.write("project (code):",pageDict['project'])
        st.write("stage (code):",pageDict['stage'])
        infra.ToggleButton(pageDict,'toggleChanger','Change default test values?')
        if pageDict['toggleChanger']:
            infra.TextBox(pageDict,'componentType',"Enter componentType code:")
            infra.TextBox(pageDict,'code',"Enter testType code:")
            infra.TextBox(pageDict,'stage',"Enter testStage _code_:")
            infra.TextBox(pageDict,'project',"Enter project code:")


        # get test schema
        if "origSchema" not in pageDict.keys() or st.button("Reset Schema: "+pageDict['code']+"@"+pageDict['stage']):
            pageDict['origSchema'] = DBaccess.DbGet(st.session_state.myClient,'generateTestTypeDtoSample', {'project':pageDict['project'], 'componentType':pageDict['componentType'], 'code':pageDict['code'], 'requiredOnly':True})

        if st.session_state.debug:
            st.write("**DEBUG** Original *schema*")
            st.write(pageDict['origSchema'])


        ### add gleaned information to test schema
        pageDict['testSchema']=pageDict['origSchema']
        pageDict['testSchema']['institution']=st.session_state.Authenticate['proj']['code']

        infra.ToggleButton(pageDict,'toggleEdit','Edit Schema?')
        if pageDict['toggleEdit']:
            st.write("### Edit Schema")
            pageDict['testSchema']=EditJson(pageDict['testSchema'])
        else:
            st.write("### Test Schema")
            st.write(pageDict['testSchema'])

        infra.ToggleButton(pageDict,'toggleText','Convert values to text?')
        if pageDict['toggleText']:
            pageDict['testSchema'] = json.loads(json.dumps(pageDict['testSchema']), parse_int=str)
            pageDict['testSchema']['results'] = json.loads(json.dumps(pageDict['testSchema']['results']), parse_float=str)
        if st.session_state.debug:
            st.write("### **DEBUG** (final!) Test Schema")
            st.write(pageDict['testSchema'])

        # upload(!) test schema: change DBaccess(get) --> DbUpdate(post)
        if st.button("Upload Test"):
            ### set stage
            try:
                pageDict['setVal']=DBaccess.DbPost(st.session_state.myClient,'setComponentStage', {'component':pageDict['testSchema']['component'], 'stage':pageDict['stage']})
                st.write("### **Successful stage set** (",pageDict['stage'],") for:",pageDict['setVal']['serialNumber'])
                # df_set=pd.json_normalize(pageDict['setVal'], sep = "_")
                # st.dataframe(df_set)
            except itkX.BadRequest as b:
                st.write("### :no_entry_sign: Stage (",pageDict['stage'],")setting **Unsuccessful**")
                st.write(str(b)[str(b).find('"message": ')+len('"message": '):str(b).find('"paramMap"')-8]) # sucks
            ### upload data
            try:
                pageDict['upVal']=DBaccess.DbPost(st.session_state.myClient,'uploadTestRunResults', pageDict['testSchema'])
                st.write("### **Successful Upload**:",pageDict['upVal']['componentTestRun']['date'])
                st.balloons()
                st.write(pageDict['upVal'])
            except itkX.BadRequest as b:
                st.write("### :no_entry_sign: Update **Unsuccessful**")
                st.write(str(b)[str(b).find('"message": ')+len('"message": '):str(b).find('"paramMap"')-8]) # sucks
        if 'upVal' in pageDict.keys():
            try:
                st.write("last successful _upload_:",pageDict['upVal']['testRun']['id'])
                if st.button("Delete testrun"):
                    ### delete data
                    try:
                        pageDict['delVal'] = DBaccess.DbPost(st.session_state.myClient,'deleteTestRun', {'testRun':pageDict['upVal']['testRun']['id']})
                        st.write("### **Successful Deletion**:",pageDict['delVal']['testRun']['id'])
                        st.balloons()
                        st.write(pageDict['delVal'])
                    except itkX.BadRequest as b:
                        st.write("### :no_entry_sign: Deletion unsuccessful")
                        st.write(str(b)[str(b).find('"message": ')+len('"message": '):str(b).find('"paramMap"')-8]) # sucks
            except KeyError:
                pass
        if 'delVal' in pageDict.keys():
            try:
                st.write("last successful _deletion_:",pageDict['delVal']['testRun']['id'])
            except KeyError:
                pass
