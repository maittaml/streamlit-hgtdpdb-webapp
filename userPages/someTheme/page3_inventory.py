### standard
import streamlit as st
from core.Page import Page
### custom
import pandas as pd
import ast
import csv
import json
import plotly.express as px
from datetime import datetime
### PDB stuff
import itkdb
import core.DBaccess as DBaccess
import core.stInfrastructure as infra
import itkdb
import itkdb.exceptions as itkX

#####################
### useful functions
#####################
def ColorCells(s, df, colName, flip=False):
    thisRow = pd.Series(data=False, index=s.index)
    colours=['red','blue','green','orange','purple''yellow','pink','lightblue','lightgreen']*3
    names=list(df[colName].unique())
    if flip:
        return ['background-color: %s ; color: %s'% ('white',colours[names.index(s[colName])])]*len(df.columns)
    else:
        return ['background-color: %s ; color: %s'% (colours[names.index(s[colName])],'black')]*len(df.columns)


infoList=["  * upload IZM _xlsx_ data file",
        "  * review test schema",
        "   * reset if required",
        "   * edit if required",
        "  * upload test schema",
        "   * delete test upload if required"]
#####################
### main part
#####################

class Page3(Page):
    def __init__(self):
        super().__init__("Inventory", ":microscope: Inventory of Components @ IZM", infoList)

    def main(self):
        super().main()

        ### getting attribute
        pageDict=st.session_state[self.name]

        ### check requirements to do stuff
        doWork=False
        try:
            if st.session_state.myClient:
                doWork=True
            if st.session_state.debug:
                st.write(":white_check_mark: Got Token")
        except AttributeError:
            st.write("No token")

        ### gatekeeping
        if not doWork:
            st.stop()

        st.write("### Check componentTypes in PDB")
        if st.button("reset list") or 'compList' not in list(pageDict.keys()): # check list
            st.write("**Getting** list for "+st.session_state.Authenticate['inst']['code']+" ("+st.session_state.Authenticate['proj']['code']+")")
            compList=st.session_state.myClient.get('listComponents', json={'project':st.session_state.Authenticate['proj']['code'], 'institution':st.session_state.Authenticate['inst']['code']})
            df_compList=pd.json_normalize(compList.data, sep = "_")
            pageDict['compList']=df_compList.groupby(["institution_code","componentType_code","state"]).sum().reset_index()
            pageDict['plotList']=pd.DataFrame({'count': pd.DataFrame(df_compList).groupby( ['institution_code','componentType_code'] ).size() }).reset_index()
        else:
            st.write("**Got** list for "+st.session_state.Authenticate['inst']['code']+" ("+st.session_state.Authenticate['proj']['code']+")")

        if 'compList' not in list(pageDict.keys()):
            st.write("No data")
            st.stop()

        st.dataframe(pageDict['compList'])
        # plotly chart
        st.write("### All project("+st.session_state.Authenticate['proj']['code']+") components @ "+st.session_state.Authenticate['inst']['name'])
        sun_chart = px.sunburst(pageDict['plotList'], path=['institution_code','componentType_code'], values='count',
                      color='count', hover_data=['count'])
        st.plotly_chart(sun_chart)

        now = datetime.now() # current date and time
        st.download_button(label="Download list", data=pageDict['compList'].to_csv(index=False),file_name="listedData_"+st.session_state.Authenticate['inst']['code']+"_"+st.session_state.Authenticate['proj']['name']+"_"+now.strftime("%Y_%m_%d")+".csv")
